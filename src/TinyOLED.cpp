/*
 * SSD1306xLED - Drivers for SSD1306 controlled dot matrix OLED/PLED 128x64 displays
 *
 * @created: 2014-08-12
 * @author: Neven Boyanov
 *
 * Source code available at: https://bitbucket.org/tinusaur/ssd1306xled
 *
 */

// ----------------------------------------------------------------------------

#include "TinyOLED.h"

// ----------------------------------------------------------------------------

// Some code based on "IIC_wtihout_ACK" by http://www.14blog.com/archives/1358

const uint8_t ssd1306_init_sequence [] PROGMEM = {	// Initialization Sequence
	0xAE,			// Display OFF (sleep mode)
	0x20, 0b00,		// Set Memory Addressing Mode
					// 00=Horizontal Addressing Mode; 01=Vertical Addressing Mode;
					// 10=Page Addressing Mode (RESET); 11=Invalid
	0xB0,			// Set Page Start Address for Page Addressing Mode, 0-7
	0xC8,			// Set COM Output Scan Direction
	0x00,			// ---set low column address
	0x10,			// ---set high column address
	0x40,			// --set start line address
	0x81, 0x3F,		// Set contrast control register
	0xA1,			// Set Segment Re-map. A0=address mapped; A1=address 127 mapped. 
	0xA6,			// Set display mode. A6=Normal; A7=Inverse
	0xA8, 0x3F,		// Set multiplex ratio(1 to 64)
	0xA4,			// Output RAM to Display
					// 0xA4=Output follows RAM content; 0xA5,Output ignores RAM content
	0xD3, 0x00,		// Set display offset. 00 = no offset
	0xD5,			// --set display clock divide ratio/oscillator frequency
	0xF0,			// --set divide ratio
	0xD9, 0x22,		// Set pre-charge period
	0xDA, 0x12,		// Set com pins hardware configuration		
	0xDB,			// --set vcomh
	0x20,			// 0x20,0.77xVcc
	0x8D, 0x14,		// Set DC-DC enable
	0xAF			// Display ON in normal mode
	
};

CustFont *oledFont;
uint8_t oledX, oledY = 0;

SSD1306Device::SSD1306Device(void){}


void SSD1306Device::begin(void)
{
	TinyWireM.begin();
	
	for (uint8_t i = 0; i < sizeof (ssd1306_init_sequence); i++) {
		ssd1306_send_command(pgm_read_byte(&ssd1306_init_sequence[i]));
	}
	clear();
}

void SSD1306Device::setFont(const CustFont *font)
{
	oledFont = font;
}

void SSD1306Device::ssd1306_send_command_start(void) {
	TinyWireM.beginTransmission(SSD1306);
	TinyWireM.write(0x00);	// write command
}

void SSD1306Device::ssd1306_send_command_stop(void) {
	TinyWireM.endTransmission();
}

void SSD1306Device::ssd1306_send_data_byte(uint8_t byte)
{
	if(TinyWireM.write(byte) == 0){
		ssd1306_send_data_stop();
		ssd1306_send_data_start();
    TinyWireM.write(byte);
	}
}

void SSD1306Device::ssd1306_send_command(uint8_t command)
{
	ssd1306_send_command_start();
	TinyWireM.write(command);
	ssd1306_send_command_stop();
}

void SSD1306Device::ssd1306_send_data_start(void)
{
	TinyWireM.beginTransmission(SSD1306);
	TinyWireM.write(0x40);	//write data
}

void SSD1306Device::ssd1306_send_data_stop(void)
{
	TinyWireM.endTransmission();
}

void SSD1306Device::setCursor(uint8_t x, uint8_t y)
{
	ssd1306_send_command_start();
	TinyWireM.write(0xb0 + y);
	TinyWireM.write(((x & 0xf0) >> 4) | 0x10); // | 0x10
	TinyWireM.write((x & 0x0f) | 0x01); // | 0x01
	ssd1306_send_command_stop();
	oledX = x;
	oledY = y;
}

void SSD1306Device::clear(void)
{
	fill(0x00);
}

void SSD1306Device::fill(uint8_t fill)
{
	uint8_t m,n;
	for (m = 0; m < 8; m++)
	{
    setCursor(0, m);
		ssd1306_send_data_start();
		for (n = 0; n < 129; n++)
		{
			ssd1306_send_data_byte(fill);
		}
		ssd1306_send_data_stop();
	}
	setCursor(0, 0);
}

size_t SSD1306Device::write(byte c) {
  if(!oledFont)
    return 1;

  uint8_t h = oledFont->height;
  uint8_t w = oledFont->width;

	if(c == '\r')
		return 1;
	if(c == '\n'){
    oledY += h;	
		if(oledY > 7) {	
			oledY = 7;
		}
		setCursor(0, oledY);
		return 1;
	}

	if(oledX > (128 - w)){
			oledY+=h;
			if ( oledY > 7)
				oledY = 7;
			setCursor(0, oledY);
  }
  int offet = ((uint8_t)c - oledFont->first) * w * h;
  for(uint8_t line = 0; line < h; line++)
  {
    ssd1306_send_data_start();
    for(uint8_t i = 0; i < w; i++){
      ssd1306_send_data_byte(pgm_read_byte(&(oledFont->bitmap[offet++])));
    }
    ssd1306_send_data_stop();
    if(h == 1){
      oledX += w;
    }
    else{
      if(line < h - 1){
        setCursor(oledX, oledY + 1);
      }
      else{
        setCursor(oledX + w, oledY - (h - 1));
      }
    }
  }
	
	return 1;
}

void SSD1306Device::bitmap(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, const uint8_t bitmap[])
{
	uint16_t j = 0;
	uint8_t y, x;
	// if (y1 % 8 == 0) y = y1 / 8; 	// else y = y1 / 8 + 1;		// tBUG :: this does nothing as y is initialized below
	//	THIS PARAM rule on y makes any adjustment here WRONG   //usage oled.bitmap(START X IN PIXELS, START Y IN ROWS OF 8 PIXELS, END X IN PIXELS, END Y IN ROWS OF 8 PIXELS, IMAGE ARRAY);
 	for (y = y0; y < y1; y++)
	{
		setCursor(x0,y);
		ssd1306_send_data_start();
		for (x = x0; x < x1; x++)
		{
			ssd1306_send_data_byte(pgm_read_byte(&bitmap[j++]));
		}
		ssd1306_send_data_stop();
	}
	setCursor(0, 0);
}

//Used from Tiny4kOLED
void SSD1306Device::off(void) {
  ssd1306_send_command(0xAE);
}

void SSD1306Device::on(void) {
  ssd1306_send_command(0xAF);
}

SSD1306Device oled;

// ----------------------------------------------------------------------------