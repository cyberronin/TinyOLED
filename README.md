TinyOLED
-----

This library is influenced by [Tiny4kOLED](https://github.com/datacute/Tiny4kOLED)  
The author there explains that their library is an adaption of [DigisparkOLED](https://github.com/digistump/DigistumpArduino/tree/master/digistump-avr/libraries/DigisparkOLED), which is a modification of another project: [SSD1306xLED library](https://bitbucket.org/tinusaur/ssd1306xled)

My library is essentially a modification of [DigisparkOLED](https://github.com/digistump/DigistumpArduino/tree/master/digistump-avr/libraries/DigisparkOLED) and [Tiny4kOLED](https://github.com/datacute/Tiny4kOLED), but uses [TinyWireM](https://github.com/adafruit/TinyWireM).

  * Added the same support for custom fonts that Tiny4kOLED did, but I had to make some modifications to some functions
  * Fixed a bug with the fill function, it didn't quite reach the full width of the OLED.
  * Had to adjust the write function to work properly with a height of 64.