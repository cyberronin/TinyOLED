#include <TinyOLED.h>
#include <TinyWireM.h>
#include <font6x8.h>


void setup() {
  // put your setup code here, to run once:
  delay(40);
  oled.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  oled.fill(0xFF); //fill screen with color
  delay(1000);
  oled.clear(); //all black
  delay(1000);
  oled.setCursor(0,0);
  oled.setFont(FONT6X8);
  //usage: oled.setCursor(X IN PIXELS, Y IN ROWS OF 8 PIXELS STARTING WITH 0);
  oled.print(F("Ronin")); //wrap strings in F() to save RAM!
  oled.setCursor(0, 1); //one row below
  oled.println(F("Wrote things!"));
  oled.print(F("Test"));

  delay(3000);
}
